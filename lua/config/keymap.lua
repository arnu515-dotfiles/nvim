-- Vanilla vim keymap. Plugin-specific keymaps go into their respective files

local opts = { noremap = true, silent = true }

-- Stay in indent mode
vim.keymap.set('v', '<', '<gv', opts)
vim.keymap.set('v', '>', '>gv', opts)

-- Use <spc>w instead of <C-w>
vim.keymap.set('n', '<leader>w', '<C-w>', vim.tbl_extend('force', opts, { desc = 'Window' }))

-- j and k should traverse through wrapped lines
vim.keymap.set({ 'n', 'x' }, 'j', 'gj', opts)
vim.keymap.set({ 'n', 'x' }, 'k', 'gk', opts)

-- <leader>y and <leader>p|P to copy/paste directly to/from the system clipboard
vim.keymap.set({ 'n', 'v' }, '<leader>y', '"+y', vim.tbl_extend('force', opts, { desc = 'Copy to system clipboard' }))
vim.keymap.set({ 'n', 'v' }, '<leader>p', '"+p', vim.tbl_extend('force', opts, { desc = 'p from system clipboard' }))
vim.keymap.set({ 'n', 'v' }, '<leader>P', '"+P', vim.tbl_extend('force', opts, { desc = 'P from system clipboard' }))

vim.keymap.set('v', 'J', ":m '>+1<CR>gv=gv")
vim.keymap.set('v', 'K', ":m '<-2<CR>gv=gv")

vim.keymap.set({ 'n', 'i', 'v' }, '<C-j>', '<CMD>cnext<CR>', vim.tbl_extend('force', opts, { desc = 'Go to next quickfix' }))
vim.keymap.set({ 'n', 'i', 'v' }, '<C-k>', '<CMD>cprevious<CR>', vim.tbl_extend('force', opts, { desc = 'Go to prev quickfix' }))
vim.keymap.set('n', '<leader>q', '<CMD>copen<CR>', vim.tbl_extend('force', opts, { desc = 'Open quickfix list' }))

vim.keymap.set('n', '<C-b>', '<C-S-^>', vim.tbl_extend('force', opts, { desc = '' }))

vim.keymap.set('n', ']t', '<CMD>tabnext<CR>', vim.tbl_extend('force', opts, { desc = 'Next tab' }))
vim.keymap.set('n', '[t', '<CMD>tabprevious<CR>', vim.tbl_extend('force', opts, { desc = 'Previous tab' }))
vim.keymap.set('n', 'gt', '<CMD>tabnew<CR>', vim.tbl_extend('force', opts, { noremap = false, desc = 'New tab' }))
vim.keymap.set('n', 'gT', '<CMD>tabclose<CR>', vim.tbl_extend('force', opts, { noremap = false, desc = 'Close tab' }))
vim.keymap.set('n', 'g0', '<CMD>tabfirst<CR>', vim.tbl_extend('force', opts, { noremap = false, desc = 'Go to first tab' }))
vim.keymap.set('n', 'g$', '<CMD>tablast<CR>', vim.tbl_extend('force', opts, { noremap = false, desc = 'Go to last tab' }))

-- turn off / on wrapping
vim.keymap.set('n', '<leader>W', ':lua vim.wo.wrap = not vim.wo.wrap<CR>', {
  noremap = true,
  silent = true,
  desc = 'Toggle line wrap',
})
