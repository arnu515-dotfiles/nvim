-- Vanilla vim options. Plugin-specific options go into their respective files

vim.opt.cmdheight = 0 -- Height of the command line. 0 replaces the status line with the command prompt when : is pressed
vim.opt.completeopt = { 'menuone', 'noselect' }
vim.opt.conceallevel = 0 -- Prevent vim from hiding quotes in json, or backticks in markdown
vim.opt.hls = false -- Don't highlight search matches (equivalent to :noh)
vim.opt.ignorecase = true -- Ignore case in search string
vim.opt.smartcase = true -- Ignores ignorecase above if we use multiple cases in search string
vim.opt.smartindent = true -- Auto indents after stuff like {
vim.opt.autoindent = true -- Carry on the indentation from previous line
vim.opt.mouse = 'a' -- Allow using mouse for all modes. Disable vim mouse temporarily by holding shift.
vim.opt.showmode = false -- Disable mode indicator  (-- INSERT -- at the bottom of the screen)
vim.opt.showtabline = 0 -- Never show tabs (1: show if atleast 2 tabs, 2: always show)
vim.opt.termguicolors = true -- Enables 24-bit RGB color
vim.opt.undofile = true -- Persistent undo
vim.opt.cursorline = false -- Highlight line of cursor
vim.opt.laststatus = 3 -- Display only one statusline at the bottom of the window
vim.opt.showcmd = false -- Do not show last executed command
vim.opt.guifont = 'monospace:h16' -- Font used for gui apps
vim.opt.title = false -- don't set window title
vim.opt.shortmess = 'filmnxtOoFc' -- :h shortmess

vim.opt.timeoutlen = 300 -- Command timeout (in ms)
vim.opt.updatetime = 100 -- Completion update time (in ms)

vim.opt.pumheight = 10 -- Pop up menu height
vim.opt.pumblend = 10 -- Transparency of the popup menu (0=opaque)

vim.opt.splitbelow = true -- :hs always inserts window below
vim.opt.splitright = true -- :vs always inserts window right

vim.opt.expandtab = false -- Do not convert tabs to spaces
vim.opt.shiftwidth = 2 -- No of spaces per indent level
vim.opt.tabstop = 2 -- A tab is how many spaces

vim.opt.relativenumber = true -- Show relative line numbers. (set to false, and set number to true to disable this (:h 'number'))
vim.opt.numberwidth = 2 -- Make the linenumbers thinner
vim.opt.number = true -- Don't show the current line's number (displays 0 instead with relative number)
vim.opt.signcolumn = 'yes' -- Show sign column always to prevent shifting of text

vim.opt.foldenable = true -- Enable folding
vim.opt.foldcolumn = '1' -- Show fold column always to prevent shifting of text
vim.opt.foldmethod = 'manual' -- Manual folding
vim.opt.foldlevel = 99 -- UFO needs this
vim.opt.foldlevelstart = 99 -- Start with all folds open

vim.cmd 'set whichwrap+=<,>,[,],h,l'
vim.cmd [[set iskeyword+=-]]

vim.g.netrw_banner = 0
vim.g.netrw_mouse = 2

vim.g.mapleader = ' '
