return {
  'stevearc/aerial.nvim',
  dependencies = {
    'nvim-treesitter/nvim-treesitter',
    'nvim-tree/nvim-web-devicons',
  },
  config = function()
    local map = function(keys, func, desc)
      vim.keymap.set('n', keys, func, { desc = 'Aerial: ' .. desc })
    end

    require('aerial').setup {
      layout = {
        max_width = { 30, 0.15 },
        min_width = { 8 },
        default_direction = 'right',
        placement = 'edge',
      },
      attach_mode = 'global',
    }

    map('<leader>ls', '<cmd>AerialToggle!<CR>', 'Show [s]ymbols')
    map('<leader>ln', '<cmd>AerialPrev<CR>', '[n]ext symbol')
    map('<leader>lp', '<cmd>AerialNext<CR>', '[p]revious symbol')
  end,
}
