return {
  'zbirenbaum/copilot.lua',
  event = 'InsertEnter',
  config = function()
    require('copilot').setup {
      panel = {
        enabled = true,
        auto_refresh = false,
        keymap = {
          jump_prev = '[[',
          jump_next = ']]',
          accept = '<CR>',
          refresh = 'gr',
          open = '<M-CR>',
        },
        layout = {
          position = 'bottom', -- | top | left | right
          ratio = 0.4,
        },
      },
      suggestion = {
        enabled = true,
        auto_trigger = true,
        debounce = 75,
        keymap = {
          accept = false,
          accept_word = false,
          accept_line = false,
          next = false,
          prev = false,
          dismiss = false,
        },
      },
    }

    local suggest = require 'copilot.suggestion'

    vim.keymap.set('i', '<M-o>a', function()
      suggest.accept()
    end, { desc = 'Accept copilot suggestion' })
    vim.keymap.set('i', '<M-o>w', function()
      suggest.accept_word()
    end, { desc = 'Accept copilot word' })
    vim.keymap.set('i', '<M-o>l', function()
      suggest.accept_line()
    end, { desc = 'Accept copilot line' })
    vim.keymap.set('i', '<M-o>n', function()
      suggest.next()
    end, { desc = 'Next copilot suggestion' })
    vim.keymap.set('i', '<M-o>p', function()
      suggest.prev()
    end, { desc = 'Prev copilot suggestion' })
    vim.keymap.set('i', '<M-o>d', function()
      suggest.dismiss()
    end, { desc = 'Dismiss copilot suggestion' })
  end,
}
