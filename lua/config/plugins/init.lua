local lazypath = vim.fn.stdpath 'data' .. '/lazy/lazy.nvim'

if not vim.loop.fs_stat(lazypath) then
  vim.fn.system {
    'git',
    'clone',
    '--filter=blob:none',
    'https://github.com/folke/lazy.nvim.git',
    '--branch=stable',
    lazypath,
  }
end
vim.opt.rtp:prepend(lazypath)

require('lazy').setup {
  spec = {
    { import = 'config.plugins.colorschemes.catppuccin' },
    { import = 'config.plugins.colorschemes.gruvbox' },
    { import = 'config.plugins.colorschemes.phoenix' },
    { import = 'config.plugins.colorschemes.kanagawa' },
    { import = 'config.plugins.colorschemes.nightfox' },
    { import = 'config.plugins.colorschemes.no-clown-fiesta' },
    { import = 'config.plugins.colorschemes.oxocarbon' },
    { import = 'config.plugins.colorschemes.tokyonight' },
    { import = 'config.plugins.colorschemes.vim-256noir' },
    { import = 'config.plugins.colorschemes.vim-paper' },

    { import = 'config.plugins.aerial' },
    { import = 'config.plugins.cmp' },
    { import = 'config.plugins.comments' },
    { import = 'config.plugins.conform' },
    { import = 'config.plugins.copilot' },
    { import = 'config.plugins.git' },
    { import = 'config.plugins.gitsigns' },
    { import = 'config.plugins.file' },
    { import = 'config.plugins.leap' },
    -- { import = 'config.plugins.llm' },
    { import = 'config.plugins.lspconfig' },
    { import = 'config.plugins.mini' },
    { import = 'config.plugins.surround' },
    { import = 'config.plugins.telescope' },
    { import = 'config.plugins.treesitter' },
    { import = 'config.plugins.trouble' },
    { import = 'config.plugins.ufo' },
    { import = 'config.plugins.undotree' },
    { import = 'config.plugins.vim-maximizer' },
    -- { import = 'config.plugins.wakatime' },
    { import = 'config.plugins.which-key' },

    { import = 'config.plugins.languages' },
  },
  install = {
    missing = true, -- install missing plugins on startup
  },
  ui = {
    border = 'rounded',
  },
}

--vim.cmd.colorscheme 'lunaperche'
vim.cmd.colorscheme 'no-clown-fiesta'
