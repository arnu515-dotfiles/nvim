return {
  'ggandor/leap.nvim',
  config = function()
    local leap = require 'leap'
    leap.opts.special_keys.prev_target = '<bs>'
    leap.opts.special_keys.prev_group = '<bs>'
    leap.opts.special_keys.next_target = '<enter>'
    leap.opts.special_keys.next_target = '<enter>'
    require('leap.user').set_repeat_keys('<cr>', '<bs>')

    vim.api.nvim_create_autocmd('User', {
      pattern = 'LeapEnter',
      callback = function()
        vim.cmd.hi('Cursor', 'blend=100')
        vim.opt.guicursor:append { 'a:Cursor/lCursor' }
      end,
    })
    vim.api.nvim_create_autocmd('User', {
      pattern = 'LeapLeave',
      callback = function()
        vim.cmd.hi('Cursor', 'blend=0')
        vim.opt.guicursor:remove { 'a:Cursor/lCursor' }
      end,
    })

    vim.keymap.set({ 'n', 'x', 'o' }, 'ss', '<Plug>(leap-forward)', { desc = 'Leap forward' })
    vim.keymap.set({ 'n', 'x', 'o' }, 'sS', '<Plug>(leap-backward)', { desc = 'Leap backward' })
    vim.keymap.set({ 'n', 'x', 'o' }, 'st', '<Plug>(leap-forward-till)', { desc = 'Leap forward till' })
    vim.keymap.set({ 'n', 'x', 'o' }, 'sT', '<Plug>(leap-backward-till)', { desc = 'Leap backward till' })
    vim.keymap.set({ 'n', 'x', 'o' }, 'gs', '<Plug>(leap-from-window)', { desc = 'Leap from window' })
  end,
}
