return {
  'kylechui/nvim-surround',
  opts = {
    keymaps = {
      insert = '<C-g>s',
      insert_line = '<C-g>S',
      normal = 'ms',
      normal_cur = 'mss',
      normal_line = 'mS',
      normal_cur_line = 'mSS',
      visual = 'ms',
      visual_line = 'mS',
      delete = 'md',
      change = 'mr',
      change_line = 'mR',
    },
  },
}
