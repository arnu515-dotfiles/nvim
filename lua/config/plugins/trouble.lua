return {
  'folke/trouble.nvim',
  branch = 'main',
  keys = {
    {
      '<leader>ltX',
      '<cmd>Trouble diagnostics toggle<cr>',
      desc = 'All Diagnostics',
    },
    {
      '<leader>ltx',
      '<cmd>Trouble diagnostics toggle filter.buf=0<cr>',
      desc = 'Buffer Diagnostics',
    },
    {
      '<leader>lts',
      '<cmd>Trouble symbols toggle focus=false<cr>',
      desc = 'Symbols',
    },
    {
      '<leader>ltL',
      '<cmd>Trouble loclist toggle<cr>',
      desc = 'Location List (Trouble)',
    },
    {
      '<leader>ltq',
      '<cmd>Trouble qflist toggle<cr>',
      desc = 'Quickfix List (Trouble)',
    },
  },
  opts = {
    focus = true, -- focus the window when opened
  },
}
