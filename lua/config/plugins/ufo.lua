return {
  'kevinhwang91/nvim-ufo',
  dependencies = 'kevinhwang91/promise-async',
  opts = {
    open_fold_hl_timeout = 200,
    close_fold_kinds_for_ft = {
      default = { 'imports' },
      json = { 'array' },
      c = { 'comment', 'region' },
    },
    preview = {
      win_config = {
        border = { '', '─', '', '', '', '─', '', '' },
        winhighlight = 'Normal:Folded',
        winblend = 0,
      },
      mappings = {
        scrollU = '<C-u>',
        scrollD = '<C-d>',
        jumpTop = '[',
        jumpBot = ']',
      },
    },
    provider_selector = function(bufnr, filetype, buftype)
      return { 'lsp', 'indent' }
    end,
  },
}
