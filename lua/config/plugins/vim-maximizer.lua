return {
  'szw/vim-maximizer',
  keys = {
    { '<C-w>m', '<cmd>MaximizerToggle<CR>', desc = '(Toggle) Maximise a split' },
    { '<leader>wm', '<cmd>MaximizerToggle<CR>', desc = '(Toggle) Maximise a split' },
  },
}
